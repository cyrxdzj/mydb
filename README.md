本软件正在开发。
# 1，简介
mydb是一款内存数据库，支持从文件导出、导入至文件。<br>
主要原理：用列表存储字典。
# 2，安装
**2-1，程序**<br>
`pip3 install mydb`<br>
**2-2,依赖**<br>
本软件没有依赖。<br>
# 3，文件目录结构
    mydb 根目录
    ╲__init__.py
    ╲main.py 主文件
# 4，使用
**4-1，类与对象**<br>
类名：database<br>
简介：数据库<br>
- \_\_init\_\_(filepath=None)<br>
简介：初始化函数。<br>
参数：<br>
filepath:指定载入文件路径，如不指定，创建一个新的表<br>
- where(key,word)<br>
简介：查询：当字典有键key并且此键对应的值为word时，返回位置。如有多次出现，返回第一次。如没有，返回-1。<br>
参数：<br>
key:键<br>
word：值<br>
返回：位置。<br>
- add(dict)<br>
简介：增加字典到列表尾部<br>
参数：<br>
dict:要增加的字典<br>
返回：没有返回。<br>
- insert(dict,index)
简介：插入。若index大于等于插入前列表长度，将执行add函数。<br>
参数：<br>
dict:要插入的字典<br>
index:要插入的位置。说得通俗一点，就是：这个字典插入后，位置在index（如果index小于插入前列表的长度）<br>
返回：没有返回。<br>
- get(index)
简介：得到列表的某一位置的字典。<br>
参数：<br>
index:位置<br>
返回：字典。<br>
注意：若index超出下标范围，抛出MydbError异常。<br>
- delete(index)
简介：删除index下标的字典。<br>
参数：<br>
index:位置<br>
返回：没有返回。<br>
- search(dict)
简介：查找字典。如有多次出现，返回第一次。如没有，返回-1。<br>
参数：<br>
dict:要查找的字典<br>
返回：位置<br>
注意：与where函数的不同点：where只查找一对，而search查找整个字典。<br>
- save(filepath="")
简介：保存。<br>
参数：<br>
filepath:保存位置。新建的数据库必选，打开的数据库可选。若不选，自动覆盖文件<br>
返回：没有返回。<br>