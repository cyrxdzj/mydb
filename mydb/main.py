import pickle
class MydbError(Exception):
    def __init__(self,data):
        self.data=data
    def __str__(self):
        return self.data
class database():
    data=[]
    path=""
    def __init__(self,filepath=None):
        if filepath!=None:
            loadfile=open(filepath)
            self.data=pickle.load(loadfile)
            loadfile.close()
            self.path=filepath
    def where(self,key,word):
        for i in range(0,len(self.data)):
            dict=self.data[i]
            if key in dict.keys:
                if dict[key]==word:
                    return i
        return -1
    def search(self,dict):
        for i in range(0,len(self.data)):
            if dict==self.data[i]:
                return i
        return -1
    def save(self,filepath=""):
        if filepath=="" and self.path=="":
            raise MydbError("An unavailable path parameter was passed in.")
        elif filepath!="":
            savefile=open(filepath)
            pickle.dump(savefile,self.data)
            savefile.close()
        else:
            savefile=open(self.path)
            pickle.dump(savefile,self.data)
            savefile.close()
    def add(self,dict):
        self.data.append(dict)
    def insert(self,dict,index):
        if index<len(self.data):
            self.data.insert(dict,index)
        else:
            self.add(dict)
    def get(self,index):
        if index>=len(self.data):
            raise MydbError("Exceeds the list subscript.")
        else:
            return self.data[index]
    def delete(self,index):
        if index>=len(self.data):
            raise MydbError("Exceeds the list subscript.")
        else:
            del self.data[index]
    def length(self):
        return len(self.data)
    def where_all(self,key,word):
        index_list=[]
        for i in range(0,len(self.data)):
            dict=self.data[i]
            if key in dict.keys():
                if dict[key]==word:
                    index_list.append(i)
        return index_list